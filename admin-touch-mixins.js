export default {
    mounted() {
        let vm = this;
        document.addEventListener('gesturestart', function (e) {
            e.preventDefault();
        });
        document.addEventListener('mousedown', this.onStartMove)
        document.addEventListener('mousemove', this.onMove)
        document.addEventListener('mouseup', this.onMoveEnd)
        /*
        var obj = this.$el;
        obj.addEventListener('touchstart', function(event) {
            if (event.targetTouches.length == 1) {
                var touch = event.targetTouches[0];
                vm.onStartMove(touch);
            }
        }, false);
        let timer = null;
        let len1 = null;
        let startScale = 1;
        obj.addEventListener('touchmove', function(event) {
            if (event.targetTouches.length == 1) {
                var touch = event.targetTouches[0];
                if (!vm.isMove) vm.onStartMove(touch); else {
                    vm.onMove(touch);
                    clearTimeout(timer)
                    timer = setTimeout(function () {
                        vm.onMoveEnd()
                    }, 50);
                }
            } else {
                /*vm.isMove = false;
                let p1 = event.targetTouches[0];
                let p2 = event.targetTouches[0];
                let len2 = Math.abs(p1.screenX - p2.screenX);
                if (!len1) {
                    len1 = len2;
                    startScale = vm.scale;
                    return;
                }
                vm.scale = startScale - len1/len2;
                //len1 = len2;
            }
        }, false);
        */
    },
    methods: {
        onStartMove(e) {
            if (this.current) return;
            this.isMove = true;
            this.startMove.x = e.x || e.clientX;
            this.startMove.y = e.y || e.clientY;
            this.currentMove.x = e.x || e.clientX;
            this.currentMove.y = e.y || e.clientY;
        },
        onMove(e) {
            if (this.isMove && e.which != 1) { window.getSelection().removeAllRanges(); }
            if (this.current || !this.isMove || (e.which  && e.which != 1)) return;
            this.currentMove.x = e.x || e.clientX;
            this.currentMove.y = e.y || e.clientY;
            //console.log(this.currentMove.x, this.startMove.x)
            let x = - this.currentMove.x + this.startMove.x;
            let y = - this.currentMove.y + this.startMove.y;
            //console.log(x)
            window.scrollTo(window.scrollX + x, window.scrollY + y)
            this.startMove.x = this.currentMove.x;
            this.startMove.y = this.currentMove.y;
        },
        async onMoveEnd() {
            this.isMove = false;
            return;
            console.log('onMoveEnd')
            if (this.current) return;

            if (this.startMove.x - this.currentMove.x == 0 && this.startMove.y - this.currentMove.y == 0) this.isMove = false;

            this.resultDelteMove.x = (this.currentMove.x - this.startMove.x) * (2 - this.scale) + this.resultDelteMove.x;
            this.resultDelteMove.y = (this.currentMove.y - this.startMove.y) * (2 - this.scale) + this.resultDelteMove.y;
            this.currentMove.x = null;
            this.currentMove.y = null;
            this.startMove.x = null;
            this.startMove.y = null;
            await this.$nextTick();
            this.isMove = false;
        },
    }
}