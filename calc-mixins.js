import {mapState, mapActions, mapMutations, mapGetters} from 'vuex'

export default {
    computed: {
        ...mapState('Calc', {Calcs: 'items'})
    },
    methods: {
        ...mapActions('Calc', {CalcLoad: 'load', CalcRemove: 'remove', CalcSave: 'save', CalcCreate: 'create'}),
        ...mapMutations('Calc', {CalcUpdate: 'update'}), // Локальные правки
        CalcUpdateLocal(calc) {
            this.$store.commit('Calc/update', calc)
        },
    }
}