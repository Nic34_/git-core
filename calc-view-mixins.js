export default {
    computed: {
        calc() {
            // item - элемент, в котором проихводится поиск данных калькулятора
            let consts = [], services = [], formula = [];
            if (this.item && this.item.calc) {
                ({consts, services, formula} = JSON.parse(this.item.calc))
                if (formula && formula.length) return {consts, services, formula}
            }
             return false;
        },
    },
    methods: {
    }
}