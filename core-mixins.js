import {mapState, mapGetters, mapActions, mapMutations} from 'vuex'

export default {
    computed: {
        ...mapState('Auth', ['isAuth', 'isAdmin', 'isDemo']),
        ...mapGetters('Permiss', ['PermissForUser']),
        Permiss() { return this.PermissForUser; },
    },
    created() {
        //if (!this.isAuth) this.$router.push('/Auth');
        //console.log(this.isAuth)
        this.$store.commit('Auth/Restore')
    },
    methods: {
        TestPermiss(Permiss) {
            if (!this.Permiss[Permiss]) {
                M.toast({html: 'Нет прав'});
                throw `Нет прав для выполенения ${Permiss}`;
            }
        },
        hasValue(value) {
            //console.log(value)
            if (value === '' || value === null || value === '0') return false;
            return value
        },
        ...mapActions('Auth',  {LoginMethod: 'Login', SignInMethod: 'SignIn'}),
        ...mapActions('Auth',  ['Logout']),
        ...mapMutations('Auth',  ['Restore']),
        Logout() {
            this.$store.commit('Auth/Logout');
            this.$store.commit('UserCalcs/clear');
            this.$router.push('/')
        },
        getNewTempId() {
            return btoa(Math.random()).substr(5, 5)
        }
    }
}