import 'es6-promise/auto'
import 'babel-polyfill'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import CKEditor from '@ckeditor/ckeditor5-vue';
import VueNestable from 'vue-nestable'

import '../node_modules/materialize-css/dist/css/materialize.min.css';
import '../node_modules/materialize-css/dist/js/materialize.min.js';

import random from './random'

Vue.use( CKEditor );
Vue.use( VueNestable );

import Sortable from 'vue-sortable'
Vue.use(Sortable)

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    if (localStorage && localStorage.isAuth) this.Restore();
  },
}).$mount('#app')
