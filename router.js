import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/admin/',
            name: 'ListOpros',
            component: () => import(/* webpackChunkName: "admin" */ './views/ListOpros.vue'),
        },
        {
            path: '/Auth',
            name: 'Auth',
            component: () => import(/* webpackChunkName: "Auth" */ './views/Auth.vue')
        },
        {
            path: '/SignIn',
            name: 'SignIn',
            component: () => import(/* webpackChunkName: "Auth" */ './views/SignIn.vue')
        },
        {
            path: '/admin/props',
            name: 'Props',
            component: () => import(/* webpackChunkName: "Auth" */ './views/AdminProps.vue')
        },
        {
            path: '/admin/templates',
            name: 'templates',
            component: () => import(/* webpackChunkName: "admin" */ './views/ListTemplates.vue')
        },
        {
            path: '/admin/roles',
            name: 'RolesCompare',
            component: () => import(/* webpackChunkName: "admin" */ './views/RolesCompare.vue')
        },
        {
            path: '/admin/calc/:id?',
            name: 'Calc',
            component: () => import(/* webpackChunkName: "admin" */ './views/Calc.vue')
        },
        {
            path: '/admin/:id/:idShow?',
            name: 'admin',
            component: () => import(/* webpackChunkName: "admin" */ './views/Admin.vue')
        },
        {
            path: '/user-area',
            name: 'UserArea',
            component: () => import(/* webpackChunkName: "admin" */ './views/UserArea.vue')
        },
        {
            path: '/user-area/calc/:id',
            name: 'UserCustomCalc',
            component: () => import(/* webpackChunkName: "admin" */ './views/UserArea.vue')
        },
        {
            path: '/store',
            name: 'store',
            component: () => import(/* webpackChunkName: "admin" */ './views/Store.vue')
        },
        {
            path: '/store/edit/:index',
            name: 'StoreCalcEdit',
            component: () => import(/* webpackChunkName: "admin" */ './views/StoreCalcEdit.vue')
        },
        {
            path: '/:idRoot?/:id?',
            name: 'home',
            component: Home
        }
    ],
})
