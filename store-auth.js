export default  {
    namespaced: true,
    state: {
        isAuth: false,
        idUser: null,
        isAdmin: false,
        isDemo: false,
        Login: '',
        token: '',
        response: {},
    },
    getters: {
    },
    mutations: {
        Login(state, {Login, isAdmin, isDemo, FIO, Email, token, idUser}) {
            state.isAdmin = isAdmin == 1;
            state.isDemo = isDemo == 1;
            state.Login = Login
            state.FIO = FIO
            state.Email = Email
            state.token = token
            state.idUser = idUser
            state.isAuth = true;
            if (window.localStorage) {
                localStorage.FIO = state.FIO;
                localStorage.Email = state.Email;
                localStorage.Login = state.Login;
                localStorage.isAdmin = state.isAdmin ? 1 : 0;
                localStorage.isDemo = state.isDemo ? 1 : 0;
                localStorage.token = state.token;
                localStorage.idUser = state.idUser;
            }
        },
        Restore(state) {
            if (window.localStorage) {
                state.FIO = localStorage.FIO || '';
                state.Email = localStorage.Email || '';
                state.Login = localStorage.Login || '';
                state.isAdmin = localStorage.isAdmin == 1;
                state.isDemo = localStorage.isDemo == 1;
                state.token = localStorage.token || '';
                state.idUser = localStorage.idUser || '';
                state.isAuth = localStorage.idUser ? true : false;
            }
        },
        Logout(state) {
            state.FIO = '';
            state.Email = '';
            state.Login = '';
            state.isAdmin = false;
            state.isDemo = false;
            state.token = '';
            state.idUser = null;
            state.isAuth = false;
            localStorage.removeItem('FIO')
            localStorage.removeItem('Email')
            localStorage.removeItem('Login')
            localStorage.removeItem('isAdmin')
            localStorage.removeItem('isDemo')
            localStorage.removeItem('token')
            localStorage.removeItem('idUser')
            localStorage.removeItem('isAuth')
        },
        Response(state, res) {
            state.Response = res;
        },
    },
    actions: {
        async Login({state, commit, rootGetters}, {Login, Pass}) {
            commit('Logout');
            let error_save = false;
            try {
                await fetch(`${rootGetters.serverUrl}/auth/login`, {method: 'post', body: JSON.stringify({Login, Pass})}).then(res => {
                    if (res.ok) return res.json(); else error_save = true;
                }).then( res => {
                    if (res.result && res.result == "1") {
                        commit('Login', {Login, ...res})
                    }
                } )
            } catch (e) {
                error_save = true;
            }
            if (error_save) { return false; }
        },
        async SignIn({state, commit, rootGetters}, {Login, Pass, FIO, Email}) {
            commit('Logout');
            let error_save = false;
            try {
                await fetch(`${rootGetters.serverUrl}/auth/create`, {method: 'post', body: JSON.stringify({Login, Pass, FIO, Email})}).then(res => {
                    if (res.ok) return res.json(); else error_save = true;
                }).then( res => {
                    if (res.result && res.result == "1") {
                        commit('Login', {Login, ...res})
                    } else {
                        commit('Response', res)
                        return false;
                    }
                } )
            } catch (e) {
                error_save = true;
            }
            if (error_save) { return false; }
        },
        Logout({state, commit, rootState}) {
            commit('Logout')
        },
    }
}