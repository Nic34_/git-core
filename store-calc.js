export default {
    namespaced: true,
    state: {
        items: []
    },
    getters: {
        getById: state => id => {
            console.log({id, items: state.items})
            let item = state.items.find(item => item.id == id)
            if (!item) item = {
                id: null,
                services: "[]",
                consts: "[]",
                formula: "[]"
            };
            item._services = JSON.parse(item.services);
            item._consts = JSON.parse(item.consts);
            item._formula = JSON.parse(item.formula);
            return item;
        },
    },
    mutations: {
        update(state, item) { // Отбор по id. Если такого id нет, то добавление. Выполняется уже после создания на сервере
            let index = state.items.findIndex(item_ => item_.id == item.id);
            if (index === -1) {
                state.items.push(item)
            } else {
                state.items[index] = {...item};
            }
        },
        remove(state, id) {
            let index = state.items.findIndex(item => item.id == id);
            state.items.splice(index, 1);
        },
    },
    actions: {
        async load({state, commit, rootState}) {
            let error_save = false;
            try {
                await fetch(`${rootState.serverUrl}/calc/index/`)
                    .then(res => {
                        if (!res.ok) error_save = true;
                        return res.json();
                    })
                    .then(items => {
                        console.log(items)
                        for (let item of items) commit('update', item)
                    })
            } catch (e) {
                error_save = true;
            }
            if (error_save) {
                return false;
            }
        },
        async create({state, dispatch, commit, rootState}, item) {
            let error_save = false;
            commit('update', item);
            try {
                return fetch(`${rootState.serverUrl}/calc/create`, {
                    method: 'post',
                    body: JSON.stringify(item)
                }).then(res => {
                    if (res.ok) {
                        return res.text();
                    } else error_save = true;
                }).then(id => {
                    if (!error_save) {
                        return id;
                    } else {
                        return error_save;
                    }
                })
            } catch (e) {
                error_save = true;
            }
            return !error_save;
        },
        async save({state, dispatch, commit, rootState}, item) {
            let error_save = false;
            commit('update', item);
            try {
                await fetch(`${rootState.serverUrl}/calc/save`, {
                    method: 'post',
                    body: JSON.stringify(item)
                }).then(res => {
                    if (res.ok) res.text(); else error_save = true;
                });
            } catch (e) {
                error_save = true;
            }
            return !error_save;
        },
        async remove({state, commit, rootState}, id) {
            let error_save = false;
            commit('remove', id);
            try {
                await fetch(`${rootState.serverUrl}/calc/remove/${id}`)
                    .then(res => {
                        if (!res.ok) error_save = true;
                    })
            } catch (e) {
                error_save = true;
            }
            return !error_save;
        },
    }
}