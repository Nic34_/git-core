import Vue from 'vue'
import Vuex from 'vuex'

import servers from "./servers";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        items: [],
        itemsRoot: [],
        itemsTemplates: [],
        itemsBeckaps: [],
        ids_remove: [],
        current: [],
        isLoad: true,
        isAuth: false,
    },
    getters: {
        parent: state => id => {
            if (id)
                return state.items.find(item => item.id == id)
            return state.items.find(item => item.idParent == null)
        },
        childrens: state => idParent => state.items.filter(item => item.idParent == idParent),
        itemById: state => id => state.items.find(item => item.id == id),
    },
    mutations: {
        update(state, item = {id: null, name: ''}) {
            let index = state.items.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.items[index] = item;
            else
                state.items.push(item)
        },
        set_is_load(state, flag) {
            state.isLoad = flag;
        },
        clear(state) {
            state.items.splice(0);
            state.itemsRoot.splice(0);
        },
        'clear-list'(state) {
            state.itemsRoot.splice(0);
        },
    },
    actions: {
        async load({commit, dispatch, getters}, id) {
            //commit('clear');
            let url = process.env.VUE_APP_server || servers.en
            await fetch(`${url}/opros/index/${id}`)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update', item)
                })
            commit('set_is_load', false)
        },
    }
})
