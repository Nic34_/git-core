export default  {
    namespaced: true,
    state: {
        opros: {
            Title: 'Опрос',
            'load': { Title: 'Загрузить опрос', Role: ['Admin', 'Demo'], },
            'open': { Title: 'Открыть опрос', Role: ['Admin', 'Demo'], },
            'create': { Title: 'Создать опрос', Role: ['Admin'], },
            'save': { Title: 'Сохранить опрос', Role: ['Admin'], },
            'remove': { Title: 'Удалить опрос', Role: ['Admin'], },
        },
        node: {
            Title: 'Элемент опроса',
            'create': { Title: 'Создать элемент', Role: ['Admin'], },
            'change': { Title: 'Изменить элемент', Role: ['Admin'], },
            'remove': { Title: 'Удалить элемент', Role: ['Admin'], },
        },
        beckap: {
            Title: 'Бекап',
            'save': { Title: 'Сохранить бекап', Role: ['Admin'] },
            'load': { Title: 'Список бекапов', Role: ['Admin'] },
            'remove': { Title: 'Удалить бекап', Role: ['Admin'] },
        },
        template: {
            Title: 'Шаблон',
            'create': { Title: 'Создать шаблон', Role: ['Admin'] },
            'load': { Title: 'Удалить шаблон', Role: ['Admin'] },
        },
        load: {
            Title: 'Загрузка файлов',
            'image': { Title: 'Загрузка изображений', Role: ['Admin'] },
            'video': { Title: 'Загрузка видео', Role: ['Admin'] },
        },
    },
    getters: {
        PermissForUser: (state , getters, rootState, rootGetter) => {
            let result = {};
            for (let keyGroup in state) {
                let group = state[keyGroup];
                for (let key in group) {
                    if (key != 'Title') {
                        if (group[key].Role.includes('Admin') && rootState.Auth.isAdmin) {
                            result[`${keyGroup}-${key}`] = true
                        }
                        if (group[key].Role.includes('Demo') && rootState.Auth.isDemo) {
                            result[`${keyGroup}-${key}`] = true
                        }
                    }
                }
            }
            return result;
        },
    },
    mutations: {
    }
}