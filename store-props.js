export default {
    namespaced: true,
    state: {
        items: []
    },
    mutations: {
        update(state, values) { // Замена значений целиком
            state.items.splice(0);
            for(let item of values)
                state.items.push(item);
        },
    },
    actions: {
        async load({state, commit, rootState}) {
            try {
                return fetch(`${rootState.serverUrl}/props/index/`)
                    .then(res => {
                        if (!res.ok) throw 'Ошибка загрузки данных';
                        return res.json();
                    })
                    .then(items => {
                        commit('update', items.map( item => ({...item, value: item.value * 1, readonly: item.readonly * 1 }) ))
                    })
            } catch (e) {
                throw 'Ошибка загрузки данных';
            }
        },
        async save({state, dispatch, commit, rootState}, items) {
            try {
                commit('update', [...items]);
                return fetch(`${rootState.serverUrl}/props/save`, {
                    method: 'post',
                    body: JSON.stringify(items)
                }).then(res => {
                    if (res.ok) res.text(); else
                        throw 'Ошибка сохранения данных';
                });
            } catch (e) {
                throw 'Ошибка сохранения данных';
            }
        },
    }
}