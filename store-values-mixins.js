import {mapState, mapGetters, mapActions, mapMutations} from 'vuex'

export default {
    data: ()=> ({
    }),
    computed: {},
    created() {
    },
    methods: {
        AppendStoreValues(idRecord, idRoot, name, desc, consts, services, formula, ServicesValues, ConstsValues) {
            let StoreValues = this.getStoreValues();
            StoreValues.push({idRecord, idRoot, name, desc, consts, services, formula, ServicesValues, ConstsValues});
            this.saveStoreValues(StoreValues);
            return true;
        },
        getStoreValues() {
            let StoreValues = []
            try {
                StoreValues = JSON.parse(localStorage.storeValues);
            } catch (e) {
            }
            return StoreValues;
        },
        saveStoreValues(StoreValues) {
            localStorage.storeValues = JSON.stringify(StoreValues);
        },
        removeStoreValue(index) {
            let StoreValues = this.getStoreValues();
            StoreValues.splice(index, 1);
            this.saveStoreValues(StoreValues);
        },
    },
    filters: {
        toRub(num) {
            var formatter = new Intl.NumberFormat('ru-RUS', {
                currency: 'RUB',
            });
            return formatter.format(Number(num));
        }
    },
}