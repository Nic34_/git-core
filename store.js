import Vue from 'vue'
import Vuex from 'vuex'
//import 'whatwg-fetch'
import Auth from './store-auth'
import Permiss from './store-permiss'
import Calc from './store-calc'
import UserCalcs from './store-user-calcs'
import Props from './store-props'

import servers from "./servers";

Vue.use(Vuex)

export default new Vuex.Store({
    modules: { Auth, Permiss, Calc, Props, UserCalcs },
    state: {
        items: [],
        itemsRoot: [],
        itemsTemplates: [],
        itemsBeckaps: [],
        ids_remove: [],
        current: [],
        isLoad: true,
        serverUrl: servers[process.env.VUE_APP_server] || servers.local,
    },
    getters: {
        beckapsForOpros: state => id => state.itemsBeckaps.filter(item => item.id),
        serverUrl: state => state.serverUrl,
    },
    mutations: {
        set_is_load(state, flag) {
            state.isLoad = flag;
        },
        update(state, item = {id: null, name: ''}) {
            let index = state.items.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.items[index] = item;
            else
                state.items.push(item)
        },
        'update-root'(state, item = {id: null, name: ''}) {
            let index = state.itemsRoot.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.itemsRoot[index] = item;
            else
                state.itemsRoot.push(item)
        },
        'update-beckaps'(state, item = {id: null, name: ''}) {
            let index = state.itemsBeckaps.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.itemsBeckaps[index] = item;
            else
                state.itemsBeckaps.push(item)
        },
        'update-template'(state, item = {id: null, name: ''}) {
            let index = state.itemsTemplates.findIndex(Item => Item.id == item.id);
            if (index > -1)
                state.itemsTemplates[index] = item;
            else
                state.itemsTemplates.push(item)
        },
        remove(state, id) {
            let index = state.items.findIndex(item => item.id == id)
            state.items.splice(index, 1);
        },
        clear(state) {
            state.items.splice(0);
            state.itemsRoot.splice(0);
        },
        'clear-list'(state) {
            state.itemsRoot.splice(0);
        },
        'cleare-remove'(state) {
            state.ids_remove.splice(0);
        },
        'clear-templates'(state) {
            state.itemsTemplates.splice(0);
        },
        'clear-beckaps'(state) {
            state.itemsBeckaps.splice(0);
        },
        'remove-beckap'(state, id) {
            let index = state.itemsBeckaps.findIndex(item => item.id == id);
            state.itemsBeckaps.splice(index, 1);
        },
        append_remove(state, id) {
            async function rec(item) {
                remove.push(item.id)
                if (item.children) {
                    for (let child of item.children)
                        rec(child)
                }
            }

            if (!(id + '').includes('.')) {
                state.ids_remove.push(id);
                let subtree = state.items.find(item => item.id == id);
                rec(subtree);
            }
        },
        'update-sort-opros'(state, items) {
            state.itemsRoot.splice(0);
            for(let index in items) {
                state.itemsRoot.push({...items[index], sort: Number(index)+1});
            }
        },
    },
    actions: {
        async 'get-props-calc'({state}) {
            return await fetch(`${state.serverUrl}/props/hasPropsCalc`)
                .then(res => res.text())
        },
        async load({commit, dispatch, state}, id) {
            commit('clear');
            await fetch(`${state.serverUrl}/opros/index/${id}`)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update', item)
                })
            commit('set_is_load', false)
        },
        async 'load-list-opros'({commit, dispatch, state}) {
            commit('clear-list');
            await fetch(`${state.serverUrl}/opros/index`,)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update-root', item)
                })
            commit('set_is_load', false)
        },
        async 'load-list-templates'({commit, dispatch, state}) {
            commit('clear-templates');
            await fetch(`${state.serverUrl}/template/index`,)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update-template', item)
                })
            commit('set_is_load', false)
        },
        async 'load-list-beckaps'({commit, dispatch, state}, id) {
            commit('clear-beckaps');
            await fetch(`${state.serverUrl}/beckaps/index/${id}`,)
                .then(res => res.json())
                .then(items => {
                    for (let item of items) commit('update-beckaps', item)
                })
            commit('set_is_load', false)
        },
        async save({state, dispatch, commit}, tree) {
            let error_save = false;
            try {
                await fetch(`${state.serverUrl}/tree/save`, {method: 'post', body: JSON.stringify(tree)}).then(res => {
                    if (res.ok) res.text(); else error_save = true;
                });
            } catch (e) {
                error_save = true;
            }
            if (error_save) { return false; }
            for (let id of state.ids_remove) { await fetch(`${state.serverUrl}/node/remove/${id}`).then(res => { if (res.ok) res.text(); else error_save = true; }); }
            if (error_save) { return false; }
            commit('cleare-remove');
            await dispatch('load', tree.idRoot);
            await dispatch('load-list-opros');

            return true;
            //let error_save = false;
            let root = {name: tree.name, desc: tree.desc, idParent: null, sort: tree.sort};
            if (Number(tree.id) != NaN) root.id = tree.id;

            let idRoot = await fetch(`${state.serverUrl}/opros/save`, {
                method: 'post',
                body: JSON.stringify(root)
            }).then(res => { if (res.ok) res.text(); else error_save = true; });

            if (!idRoot) { return false; }
            if (error_save) { return false; }

            await fetch(`${state.serverUrl}/opros/save`, {
                method: 'post',
                body: JSON.stringify({...root, idRoot, idParent: null})
            }).then(res => { if (res.ok) res.text(); else error_save = true; });

            if (error_save) { return false; }

            for (let id of state.ids_remove) {
                await fetch(`${state.serverUrl}/opros/remove/${id}`).then(res => { if (res.ok) res.text(); else error_save = true; });
            }

            if (error_save) { return false; }

            async function rec(item, idParent, idRoot) {
                if (error_save) { return false; }
                return new Promise(async resolve => {
                    let toSave = {name: item.name, desc: item.desc, img: item.img, idParent, idRoot, to: item.to, VideoSmall: item.VideoSmall, VideoBig: item.VideoBig};
                    if (item.id && Number(item.id) != NaN) toSave.id = item.id;
                    let id = await fetch(`${state.serverUrl}/opros/save`, {
                        method: 'post',
                        body: JSON.stringify(toSave)
                    }).then(res => { if (res.ok) res.text(); else error_save = true; });

                    if (item.children) {
                        for (let child of item.children)
                            await rec(child, id, idRoot)
                    }
                    resolve();
                })
            }

            if (tree.children) {
                for (let child of tree.children)
                    await rec(child, idRoot, idRoot)
            }

            commit('cleare-remove');
            await dispatch('load', idRoot);
            await dispatch('load-list-opros');
        },
        async create({dispatch, state, commit}) {
            let id = await fetch(`${state.serverUrl}/opros/create`).then(r => r.text())
            dispatch('load-list-opros');
            return id;
        },
        async 'create-by-template'({dispatch, state, commit}, template) {
            console.log({dispacth: 'create-by-template', template})

            let tree = JSON.parse(template.tree);

            let root = {name: tree.name, desc: tree.desc, idParent: null};

            let idRoot = await fetch(`${state.serverUrl}/opros/createRoot`, {
                method: 'post',
                body: JSON.stringify(root)
            }).then(r => r.text())

            async function rec(item, idParent) {
                delete item.extend;
                let children = item.children;
                delete item.children;
                delete item.id;
                delete item.idParent;

                let id = await fetch(`${state.serverUrl}/opros/createItem`, {
                    method: 'post',
                    body: JSON.stringify({...item, idParent, idRoot})
                }).then(r => r.text())

                if (children) {
                    for (let child of children)
                        await rec(child, id)
                }
            }

            if (tree.children) {
                for (let child of tree.children)
                    await rec(child, idRoot)
            }
            await dispatch('load', idRoot);
            return idRoot
        },
        async removeOpros({dispatch, state}, tree) {
            console.log({dispatch: 'removeOpros', tree})
            let remove = [];

            async function rec(item) {
                remove.push(item.id)
                if (item.children) {
                    for (let child of item.children)
                        rec(child)
                }
            }

            rec(tree);

            for (let id of remove)
                await fetch(`${state.serverUrl}/opros/remove/${id}`,)

            return dispatch('load-list-opros');
        },
        async saveBeckap({state}, {tree, idOpros}) {
            return fetch(`${state.serverUrl}/beckaps/create`, {
                method: 'post',
                body: JSON.stringify({tree: JSON.stringify(tree), idOpros})
            })
        },
        async restoreBeckap({state}, id) {
            return fetch(`${state.serverUrl}/beckaps/restore/${id}`, { method: 'post' })
        },
        async removeBeckap({state, commit}, id) {
            commit('remove-beckap', id);
            return fetch(`${state.serverUrl}/beckaps/remove/${id}`, { method: 'post' })
        },
        async saveTemplate({state}, {tree, idOpros}) {
            return fetch(`${state.serverUrl}/template/create`, {
                method: 'post',
                body: JSON.stringify({tree: JSON.stringify(tree), idOpros})
            })
        },
        async updateSort({state, commit}, items) {
            if (!items.length) return;
            commit('update-sort-opros', items);
            return await fetch(`${state.serverUrl}/opros/updateSort`, {
                method: 'post',
                body: JSON.stringify(state.itemsRoot)
            })
        }
    }
})
